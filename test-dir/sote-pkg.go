package test_dir

import (
	"log"
	"strings"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
)

const (
	// Environments
	staging     = "staging"
	development = "development"
	demo        = "demo"
	production  = "production"
	// Variables
	dbPasswordKey = "DATABASE_PASSWORD"
	dbHostKey     = "DB_HOST"
	dbUserKey     = "DB_USERNAME"
	dbPortKey     = "DB_PORT"
	dbNameKey     = "DB_NAME"
	dbSSLModeKey  = "DB_SSL_MODE"
	noKey         = ""
)

var (
	defaultPath                = "/sote/api/"
	setToTrue           bool   = true
	pTrue               *bool  = &setToTrue
	maxResult           int64  = 10
	pMaxResult          *int64 = &maxResult
	sSSMParameterValues *ssm.GetParametersByPathOutput
	awsService          *ssm.SSM
)

func init() {

	sess, err := session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	})
	if err != nil {
		log.Fatalln(err)
	}
	awsService = ssm.New(sess)
}

func initParameters(tAWS, key string) {

	var (
		ssmPath           ssm.GetParametersByPathInput
		err               error
		queryPath         string  = defaultPath + tAWS
		pQueryPath        *string = &queryPath
		queryPathWithKey  string  = defaultPath + tAWS + key
		pQueryPathWithKey *string = &queryPathWithKey
	)
	if len(key) == 0 {
		ssmPath.Path = pQueryPath
	} else {
		ssmPath.Path = pQueryPathWithKey
	}
	ssmPath.Recursive = pTrue
	ssmPath.WithDecryption = pTrue
	ssmPath.MaxResults = pMaxResult
	sSSMParameterValues, err = awsService.GetParametersByPath(&ssmPath)
	if err != nil {
		log.Fatalln(err)
	}
}

func GetParameters(tAWS string) (parameters []*ssm.Parameter, found bool) {

	initParameters(tAWS, noKey)
	if len(sSSMParameterValues.Parameters) > 0 {
		parameters = sSSMParameterValues.Parameters
	}

	return
}
func GetDBHost(tAWS string) (dbHost string, found bool) {

	initParameters(tAWS, dbHostKey)
	dbHost, found = search(tAWS, dbHostKey)

	return
}

func search(tAWS, key string) (value string, found bool) {

	found = false
	for _, v := range sSSMParameterValues.Parameters {
		if strings.Contains(*v.Name, tAWS) {
			if strings.Contains(*v.Name, key) {
				found = true
				value = *v.Value
				break
			}
		}
	}

	return
}