package test_dir

import (
	"testing"
)

const (
	tAWS   string = "demo"
	dbHost string = "sote-backend-demo.cyqkmy6t8tbb.eu-west-1.rds.amazonaws.com"
)

func TestGetDBPassword(t *testing.T) {

	if _, found := GetDBHost(tAWS); !found {
		var p = make([]interface{}, 1)
		p[0] = dbHost
		t.Errorf("TestGetDBPassword should have found the dbHost: %v", dbHost)
	}
}
